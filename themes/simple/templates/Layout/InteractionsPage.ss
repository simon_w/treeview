<div class="content-container">
	<article>
		<div class="content">
			<ul class='treeUL interactionsUL'>
				<% loop $Interactions %>
					<li class="$FirstLast $Type">
						<% if $Type = 'follow' %>
							<% include InteractionUsers %> started following you.
						<% else_if $Type = 'reply' %>
							<% include InteractionUsers %> replied to
							<% with $Post %>
								<% include Post ShowThreadLink=1 %>
							<% end_with %>
						<% else_if $Type = 'repost' %>
							<% include InteractionUsers %> reposted
							<% with $Post %>
								<% include Post ShowThreadLink=1 %>
							<% end_with %>
						<% else_if $Type = 'star' %>
							<% include InteractionUsers %> starred
							<% with $Post %>
								<% include Post ShowThreadLink=1 %>
							<% end_with %>
						<% end_if %>
					</li>
				<% end_loop %>
			</ul>
		</div>
	</article>
</div>
<% include SideBar %>
