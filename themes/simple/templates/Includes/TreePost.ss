<li class="<% if $Children %>node $OpenClosed depth-$Depth<% if $Children.Count = 1 %> single-child<% end_if %><% else %>leaf<% end_if %>">
	<% include Post %>
	<% if $Children %>
		<ul>
			<% loop $Children %>
				<% include TreePost %>
			<% end_loop %>
		</ul>
	<% end_if %>
</li>
