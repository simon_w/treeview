<% if not $HideHeader %>
<header class="header" role="banner">
	<div class="inner">
		<a href="$BaseHref" id='LogoImage' rel="home"><img src='$ThemeDir/images/logo_small.png' width='48' height='48' alt='TreeView' /></a>
		<a href="$BaseHref" class="brand" rel="home">
			<h1>$SiteConfig.Title</h1>
		</a>
		<span class="search-dropdown-icon">L</span>
		<div class="search-bar">
			<form action="$List('SearchPage').First.Link" method='GET'>
				<input class='text' name='q' /><input class='action' type='submit' value='L' />
			</form>
		</div>
		<% include Navigation %>
	</div>
</header>
<% end_if %>
