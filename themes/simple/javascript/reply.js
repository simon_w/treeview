jQuery(function($) {
	var url = $('#Form_ReplyForm').attr('action') + '/field/' + $('#Form_ReplyForm textarea.textandimage').attr('name') + '/image';
	var regex = /\.(jpe?g|png|gif|tiff)$/i;
	$('#Form_ReplyForm').on('click', '.taxtandimageUpload', function() {
		var img = $(this).find('img');
		var id = img.data('id');
		if(id) {
			var url = $('#Form_ReplyForm').attr('action') + '/field/' + $('#Form_ReplyForm textarea.textandimage').attr('name') + '/removeImage';
			$.post(url, {
				'ID': id,
				SecurityID: $('#Form_ReplyForm_SecurityID').val()
			});
			$(this).remove();
		}
	}).submit(function() {
		var count = $('#Form_ReplyForm .taxtandimageUpload div').length;
		if(count > 0) {
			if(count === 1) {
				alert("An image is still uploading. Please wait for it to finish before submitting.");
			} else {
				alert(count + " images are still uploading. Please wait for them to finished before submitting.");
			}
			return false;
		}
		var IDs = '';
		$('#Form_ReplyForm .taxtandimageUpload img').each(function() {
			IDs += $(this).data('id') + ',';
		})
		IDs = IDs.substr(0, IDs.length-1);
		$('<input>').attr('name', 'imageIDs').attr('type', 'hidden').attr('value', IDs).appendTo($('#Form_ReplyForm'));
		return true;
	}).fileupload({
		url: url,
		dropZone: $('#Form_ReplyForm .dragdropzone'),
		fileInput: null,
		formData: { SecurityID: $('#Form_ReplyForm_SecurityID').val() },
		dataType: 'json',
		done: function(e, data) {
			data.context.find('div').remove();
			if(data.result.isError) {
				alert(data.result.error_message);
				data.context.remove();
			} else {
				data.context.find('img').data('id', data.result.id);
			}
			data.context.data('jq', null);
		},
		submit: function(e, data) {
			var maxSize = 25 * 1024 * 1024; // 25 megs
			for(var i in data.files) {
				var file = data.files[i];
				if(!regex.test(file.name)) {
					alert(file.name + " is not a JPEG, PNG or GIF.");
					data.context.remove();
					return false;
				}
				if(file.size > maxSize) {
					alert(file.name + " is bigger than 25 MB.");
					data.context.remove();
					return false;
				}
			}
			return true;
		},
		add: function(e, data) {
			data.context = $('<span>').addClass('taxtandimageUpload').append('<div>');
			data.context.appendTo($('#Form_ReplyForm textarea.textandimage').parent());
			data.context.data('jq', data.submit());
			if(regex.test(data.files[0].name)) {
				window.loadImage(data.files[0], function(img) {
					if(img.type === 'error') return;
					data.context.prepend(img);
				}, {
					maxWidth: 100
				});
			}
		},
		progress: function(e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			data.context.find('div').css('width', progress + 'px');
		},
		paramName: 'file'
	});
});
