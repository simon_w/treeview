jQuery(function($) {
	$('.treeUL li.node .triangle, .treeUL li.node .post-header').click(function(e) {
		if($(e.target).closest('a').length > 0) return true;
		var li = $(e.target).closest('li'), next;
		li.toggleClass('open closed')
		while(li.hasClass('.single-child')) {
			li = li.find('> ul > li.single-child').toggleClass('open closed');
		}
		checkAllExpands();
		return false;
	});
	$('.treeUL').on('click', '.expandLink', function() {
		$(this).closest('li').removeClass('closed').addClass('open').find('.closed').toggleClass('open closed');
		$(this).text('Close all replies').toggleClass('expandLink contractLink');
		checkAllExpands();
		return false;
	}).on('click', '.contractLink', function() {
		$(this).closest('li').find('.open').toggleClass('open closed');
		$(this).text('Expand all replies').toggleClass('expandLink contractLink');
		checkAllExpands();
		return false;
	});
	$('body').on('click', '.replyLink', function () {
		// Display an external page using an iframe
		var src = $(this).attr('href');
		$.modal('<iframe src="' + src + '" height="550" width="500" style="border:0"/>', {
			closeHTML:"",
			containerCss:{
				background: '#fff url(themes/simple/images/ajax-loader.gif) no-repeat center center',
				borderColor:"#fff",
				height:553,
				padding:0,
				width:500
			},
			overlayClose:true
		});
		return false;
	}).on('click', '.starLink, .repostLink', function () {
		var t = $(this), src = t.attr('href');
		if(src) {
			t.removeAttr('href');
			$.post(src, {}, 'json').fail(function () {
				t.attr('href', src);
				alert(t.text() + ' failed.');
			}).done(function (data) {
				if(typeof data === "string") {
					data = $.parseJSON(data);
				}
				t.attr('href', data.link).text(data.text);
			});
		}
		return false;
	});

	var expandedLinks = $('.expandLink');

	function checkAllExpands() {
		expandedLinks.each(function() {
			var li = $(this).closest('li');
			if(li.find('ul').length == 1) {
				var p = $(this).parent()[0];
				$(this).remove();
				p.normalize();
				p.removeChild(p.childNodes[p.childNodes.length-1]);
				return;
			}
			if(li.find('.closed').length === 0) {
				$(this).text('Close all replies').attr('class', 'contractLink');
			} else if(li.find('.open').length == 0) {
				$(this).text('Expand all replies').attr('class', 'expandLink');
			}
		});
	}

	$('#Form_ReplyForm').submit(function() {
		$('#Form_ReplyForm_action_doPost').attr('disabled', true);
	});

	$('.treeUL .single-child > ul > .single-child > ul').each(function() {
		$(this).addClass('single').parent().addClass('hide-tri').filter('.closed').find('> .post-container .triangle').click();
	});

	$('.treeUL ul.single > li.single-child, .treeUL .hide-tri').each(function() {
		$(this).find('> .post-container .expandLink, > .post-container .contractLink').each(function() {
			var p = $(this).parent()[0];
			$(this).remove();
			p.normalize();
			p.removeChild(p.childNodes[p.childNodes.length-1]);
		});
		$(this).find('> .post-container .post-header, > .post-container .triangle').unbind('click');
	});

	checkAllExpands();
	expandedLinks = $('.expandLink,.contractLink');

	var msgField = $('#Form_ReplyForm_Message');
	var charsLeft = document.getElementById('charsLeft');

	if(charsLeft) {
		msgField.keyup(function() {
			var base = parseInt(charsLeft.getAttribute('data-base'), 10);
			var value = base - msgField.val().length;
			charsLeft.textContent = value;
			if(value < 0) {
				charsLeft.className = 'over';
			} else if(value == 0) {
				charsLeft.className = 'at';
			} else {
				charsLeft.className = '';
			}
		}).keyup();
	}

	if($('#loadMore').length === 1) {
		$('#loadMore').click(function() {
			if($(this).text() !== 'Loading...') {
				$(this).text('Loading...').parent().addClass('loading');
				var lastDiv = $('.post-container').last();
				var postID = lastDiv.attr('id').substr(1);
				var args = {};
				if(document.location.search) {
					var str = document.location.search.substr(1);
					if(str) {
						var parts = str.split('&');
						for(var i = 0; i < parts.length; ++i) {
							var parsed = parts[i].split('=');
							var key = parsed.shift();
							args[key] = decodeURIComponent(parsed.join('='));
						}
					}
				}
				$.get(document.location.pathname + '/load/' + postID, args, function(data) {
					lastDiv.parent().removeClass('last').after(data);
					if($('.post-container.last').length !== 0 || !data) {
						$('#LoadMore').remove();
						$(window).unbind('scroll');
					} else {
						$('#loadMore').text('Load more...').parent().removeClass('loading');
					}
				});
			}
			return false;
		});

		$('#LoadMore').click(function() {
			$('#loadMore').click();
			return false;
		})

		if($('.post-container.last').length === 0) {
			$(window).scroll(function () {
				if((($(window).scrollTop()+$(window).height())+500)>=$(document).height()) {
					$('#loadMore').click();
				}
			});
		}
	}

	$('.treeUL').on('click', '.pocketLink', function() {
		var url = $(this).data('href');
		var token = getSecurityID();
		var replace = $('<span>').text('Saving...');
		$.post('PocketController/SubmitURL', {
			SecurityID: token,
			url: url
		}, function() {
			replace.text('Saved!');
			setTimeout(function() { replace.remove(); }, 1000);
		});
		$(this).replaceWith(replace);
	});
});
