<?php

class TextAndImageField extends TextareaField {
	public function Field($properties = array()) {
		Requirements::javascript(THIRDPARTY_DIR . '/jquery/jquery.js');
		Requirements::javascript(TEXTIMAGE_DIR . '/javascript/load-image.js');
		Requirements::javascript(TEXTIMAGE_DIR . '/javascript/jquery.ui.widget.js');
		Requirements::javascript(TEXTIMAGE_DIR . '/javascript/jquery.iframe-transport.js');
		Requirements::javascript(TEXTIMAGE_DIR . '/javascript/jquery.fileupload.js');

		return parent::Field($properties);
	}

	private static $allowed_actions = array(
		'image',
		'removeImage',
	);

	public function removeImage(SS_HTTPRequest $req) {
		$token = SecurityToken::inst();
		if(!$token->checkRequest($req)) {
			$res = array(
				'isError' => true,
				'error_message' => 'Your session has timed out. Please refresh and try again.'
			);
			return json_encode($res);
		}
		$id = (int)$req->postVar('ID');
		if(Session::get('TextAndImageField.Token.' . $id)) {
			$url = 'https://alpha-api.app.net/stream/0/files/' . $id;
			$ch = AppDotNet::getCH();
			curl_setopt_array($ch, array(
				CURLOPT_URL => $url,
				CURLOPT_CUSTOMREQUEST => 'DELETE',
			));
			return curl_exec($ch);
		}
		return 'Nahnahnah';
	}

	public function image(SS_HTTPRequest $req) {
		$token = SecurityToken::inst();
		if(!$token->checkRequest($req)) {
			$res = array(
				'isError' => true,
				'error_message' => 'Your session has timed out. Please refresh and try again.'
			);
			return json_encode($res);
		}
		$upload = new Upload;
		$val = $upload->getValidator();
		$val->setAllowedMaxFileSize(25*1024*1024);
		$val->setAllowedExtensions(array('jpg', 'jpeg', 'png', 'gif', 'tiff'));
		if($val->validate($_FILES['file']) && empty($_FILES['file']['error'])) {
			if($_FILES['file']['name'][0] == '@') {
				$_FILES['file']['name'][0] = '+';
			}
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$curl = curl_init('https://alpha-api.app.net/stream/0/files');
			curl_setopt_array($curl, array(
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_HEADER => true,
			));
			$headers = array(
				'Authorization: Bearer ' . AppDotNet::getAccessToken(),
				'Connection: Keep-Alive',
				'Keep-Alive: 300',
			);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

			$type = finfo_file($finfo, $_FILES['file']['tmp_name']);

			$post = array(
				'content' => '@' . $_FILES['file']['tmp_name'] . ';type=' . $type . ';filename=' . $_FILES['file']['name'],
				'type' => 'us.treeview.image',
			);

			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
			$data = curl_exec($curl);
			list(, $headers, $data) = explode("\r\n\r\n", $data, 3);
			$datas = json_decode($data);
			if(!$datas || $datas->meta->code != 200) {
				// var_dump($datas->meta);
				$res = $datas->meta;
				$res->isError = true;
				return json_encode($res);
			}
			$id = $datas->data->id;
			$token = $datas->data->file_token;
			Session::set('TextAndImageField.Token.' . $id, $token);
			Session::save();
			$res = array(
				'isError' => false,
				'id' => $id,
			);
			return json_encode($res);
		}
		$res = array(
			'isError' => true,
			'error_message' => 'Unsupported file type'
		);
		return json_encode($res);
	}
}
