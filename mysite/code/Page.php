<?php
class Page extends SiteTree {

	private static $db = array(
	);

	private static $has_one = array(
	);

}
class Page_Controller extends ContentController {
	use adn;

	private static $allowed_actions = array (
		'index',
		'logout',
		'thread',
	);

	public function Menu($level = 0) {
		$menu = parent::Menu($level);
		if($this->user && $level == 1) {
			$menu->push(new ArrayData(array(
				'Link' => $this->Link('logout'),
				'Title' => 'Logout',
				'MenuTitle' => 'Logout',
			)));
		}
		return $menu;
	}

	public function logout(SS_HTTPRequest $req) {
		Cookie::force_expiry('TreeView');
		Session::clear_all();
		return $this->redirect('home/');
	}

	public function init() {
		parent::init();

		Requirements::set_combined_files_folder('themes/simple/_combined');

		Requirements::combine_files('css.css', array(
			'themes/simple/css/reset.css',
			'themes/simple/css/layout.css',
			'themes/simple/css/typography.css',
			'themes/simple/css/form.css',
			'themes/simple/select2/select2.css',
		));

		Requirements::block('framework/thirdparty/jquery/jquery.js');

		Requirements::combine_files('library.js', array(
			'themes/simple/javascript/jquery.js',
			'themes/simple/javascript/jquery-ui.js',
			'themes/simple/javascript/jquery.entwine.js',
			'themes/simple/javascript/jquery.simplemodal.js',
			'themes/simple/select2/select2.js',
		));

		Requirements::combine_files('tree.js', array(
			'themes/simple/javascript/tree.js',
			'themes/simple/javascript/script.js'
		));

		$token = SecurityToken::getSecurityID();

		Requirements::customScript("function getSecurityID() { return '$token'; }");

		Requirements::javascript('https://d2zh9g63fcvyrq.cloudfront.net/adn.js');

		$this->adnINIT();
	}

	public function HideHeader() {
		return (bool)$this->request->requestVar('embed');
	}

	public function TreePage() {
		return TreePage::get()->First();
	}
}

if(!defined('ENT_HTML5')) define('ENT_HTML5', 0);
