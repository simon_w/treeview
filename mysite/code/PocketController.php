<?php

class PocketController extends Controller {
	private static $allowed_actions = array(
		'Connect',
		'doConnect',
		'RemovePocket',
		'SubmitURL',
	);

	const key = '10539-c8cd27624e7e15fbbdff807d';

	protected function parseRawHeaders($rawHeaders) {
		$headers = array();
		$fields = explode("\r\n", preg_replace('/\x0D\x0A[\x09\x20]+/', ' ', $rawHeaders));
		foreach( $fields as $field ) {
			if( preg_match('/([^:]+): (.+)/m', $field, $match) ) {
				$match[1] = preg_replace_callback('/(?<=^|[\x09\x20\x2D])./', function($m) { return strtoupper($m[0]); }, strtolower(trim($match[1])));
				if( isset($headers[$match[1]]) ) {
					if (!is_array($headers[$match[1]])) {
						$headers[$match[1]] = array($headers[$match[1]]);
					}
					$headers[$match[1]][] = $match[2];
				} else {
					$headers[$match[1]] = trim($match[2]);
				}
			}
		}
		return $headers;
	}

	public function Connect(SS_HTTPRequest $req) {
		$ch = curl_init('https://getpocket.com/v3/oauth/request');
		$return = Director::absoluteURL('PocketController/doConnect');
		$postData = sprintf('consumer_key=%s&redirect_uri=%s&state=%s', self::key, $return, SecurityToken::getSecurityID());
		curl_setopt_array($ch, array(
			CURLOPT_POSTFIELDS => $postData,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HEADER => true
		));

		$data = curl_exec($ch);

		$length = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

		$headers = substr($data, 0, $length);
		$headers = $this->parseRawHeaders($headers);
		$data = substr($data, $length);

		parse_str($data, $data);

		if(empty($data['code']) || $data['state'] !== SecurityToken::getSecurityID()) {
			$errorPage = DataObject::get_one('ErrorPage', "\"ErrorCode\" = 500");
			$errorPage->Content = '<p>Pocket returned an unknown error. Please try again later.</p>' .
			'<p>Pocket returned "' . $headers['X-Error'] . '"</p>';
			return ModelAsController::controller_for($errorPage)->handleRequest(new SS_HTTPRequest('GET', ''), DataModel::inst());
		}
		$parts = array(
			'request_token' => $data['code'],
			'redirect_uri' => $return
		);
		$query = http_build_query($parts);
		$url = 'https://getpocket.com/auth/authorize?' . $query;

		Session::set('PocketCode', $data['code']);

		return $this->redirect($url);
	}

	public function doConnect(SS_HTTPRequest $req) {
		$code = Session::get('PocketCode');
		if(!$code) {
			$errorPage = DataObject::get_one('ErrorPage', "\"ErrorCode\" = 404");
			$errorPage->Content = 'Pocket authorisation code not found. <a href="PocketController/Connect">Please try again</a>.';
			return ModelAsController::controller_for($errorPage)->handleRequest(new SS_HTTPRequest('GET', ''), DataModel::inst());
		}
		$ch = curl_init('https://getpocket.com/v3/oauth/authorize');
		$postData = sprintf('consumer_key=%s&code=%s', self::key, $code);
		curl_setopt_array($ch, array(
			CURLOPT_POSTFIELDS => $postData,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HEADER => true
		));

		$data = curl_exec($ch);

		$length = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

		$headers = substr($data, 0, $length);
		$headers = $this->parseRawHeaders($headers);
		$data = substr($data, $length);
		$settingsPage = SettingsPage::get()->first();
		
		if(curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200) {
			switch($headers['X-Error-Code']) {
				case 158:
					return $this->redirect($settingsPage->AbsoluteLink());
					break;
				case 185:
				case 182:
				case 159:
					$errorPage = DataObject::get_one('ErrorPage', "\"ErrorCode\" = 404");
					$errorPage->Content = 'Pocket authorisation code not found. <a href="PocketController/Connect">Please try again</a>.';
					return ModelAsController::controller_for($errorPage)->handleRequest(new SS_HTTPRequest('GET', ''), DataModel::inst());
					break;
				default:
					$errorPage = DataObject::get_one('ErrorPage', "\"ErrorCode\" = 500");
					$errorPage->Content = '<p>Pocket returned an unknown error. Please try again later.</p>' .
					'<p>Pocket returned "' . $headers['X-Error'] . '"</p>';
					return ModelAsController::controller_for($errorPage)->handleRequest(new SS_HTTPRequest('GET', ''), DataModel::inst());
			}
		}
		parse_str($data, $data);
		if(empty($data['access_token'])/* || $data['state'] !== SecurityToken::getSecurityID()*/) {
			$errorPage = DataObject::get_one('ErrorPage', "\"ErrorCode\" = 500");
			$errorPage->Content = '<p>Pocket returned an unknown error. Please try again later.</p>' .
			'<p>Pocket returned "' . $headers['X-Error'] . '"</p>';
			return ModelAsController::controller_for($errorPage)->handleRequest(new SS_HTTPRequest('GET', ''), DataModel::inst());
		}

		$user = User::get_user();
		$user->PocketName = $data['username'];
		$user->PocketToken = $data['access_token'];
		$user->write();
		return $this->redirect($settingsPage->AbsoluteLink());
	}

	public function RemovePocket(SS_HTTPRequest $req) {
		if(!SecurityToken::inst()->checkRequest($req)) return $this->httpError(400);

		$user = User::get_user();
		$user->PocketToken = $user->PocketName = null;
		$user->write();
		return $this->redirectBack();
	}

	public function SubmitURL(SS_HTTPRequest $req) {
		if(!SecurityToken::inst()->checkRequest($req)) return $this->httpError(400);
		$user = User::get_user();

		if(!$user->HasPocket()) {
			return;
		}

		$url = $req->postVar('url');
		$ch = curl_init('https://getpocket.com/v3/add');
		$postData = 'url=%s&consumer_key=%s&access_token=%s';
		$postData = sprintf($postData, rawurlencode($url), self::key, $user->PocketToken);
		curl_setopt_array($ch, array(
			CURLOPT_POSTFIELDS => $postData,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HEADER => true
		));

		$data = curl_exec($ch);

		return;
	}
}
