<?php

class TreePage extends Page {
	private static $db = array(
	);

	private static $has_one = array(
	);

	public function Content() {
		$content = $this->Content;
		$content = str_replace('$BookmarkLink', htmlentities('javascript:document.location="http://treeview.us/home/Form?postID="+encodeURIComponent(document.location.href)'), $content);
		$f = DBField::create_field('HTMLText', $content);
		$f->setOptions(['shortcodes' => false]);
		return $f;
	}
}

class TreePage_Controller extends Page_Controller {
	private static $allowed_actions = array (
		'reply',
		'login',
		'Form',
		'ReplyForm',
		'star',
		'repost',
	);

	public function star(SS_HTTPRequest $req) {
		$postID = (int)$req->param('ID');
		$delete = (bool)$req->param('OtherID');

		$url = sprintf('https://alpha-api.app.net/stream/0/posts/%d/star', intval($postID));

		if($delete) {
			AppDotNet::deleteData($url);
		} else {
			AppDotNet::postData($url, []);
		}

		if(!$req->isAjax()) {
			return $this->redirectBack();
		} else {
			return Convert::raw2json(array(
				'link' => $this->Link('star/' . $postID . '/' . (int)(!$delete)),
				'starred' => !$delete,
				'text' => $delete ? 'Star' : 'Un-star'
			));
		}
	}

	public function repost(SS_HTTPRequest $req) {
		$postID = $req->param('ID');
		$delete = (bool)$req->param('OtherID');

		$url = sprintf('https://alpha-api.app.net/stream/0/posts/%d/repost', intval($postID));

		if($delete) {
			AppDotNet::deleteData($url);
		} else {
			AppDotNet::postData($url, []);
		}

		if(!$req->isAjax()) {
			return $this->redirectBack();
		} else {
			return Convert::raw2json(array(
				'link' => $this->Link('repost/' . $postID . '/' . (int)(!$delete)),
				'reposted' => !$delete,
				'text' => $delete ? 'Repost' : 'Un-repost'
			));
		}
	}

	public function Form() {
		if($this->response->getStatusCode() != 200) {
			return '';
		}
		$fields = new FieldList(
			new TextField('postID', 'Post ID'),
			new LiteralField('blah', 'This can either be the raw post ID, the alpha.app.net URL or the posts.app.net URL.')
		);
		$actions = new FieldList(
			new FormAction('doThread', 'View thread')
		);
		$form = new Form($this, __FUNCTION__, $fields, $actions, new TreeFormVal);
		return $form->disableSecurityToken();
	}

	public function ReplyForm() {
		$fields = new FieldList(
			new TextAndImageField('Message', false),
			new HiddenField('PostID', false),
			new LiteralField('charsLeft', '<span id="charsLeft" data-base=256>256</span>'),
			new HiddenField('embed', false, $this->request->requestVar('embed'))
		);
		$actions = new FieldList(
			new FormAction('doPost', 'Post')
		);
		return new Form($this, __FUNCTION__, $fields, $actions, new PostFormVal);
	}

	public function doPost($data, $form) {
		$ids = $data['imageIDs'];
		$images = array();
		if($ids) {
			$ids = explode(',', $ids);
			$ids = array_map('intval', $ids);
			foreach($ids as $i) {
				$token = Session::get('TextAndImageField.Token.' . $i);
				if(!$token) {
					continue;
				}
				$images[] = array(
					'file_id' => $i,
					'file_token' => $token,
					'format' => 'oembed',
				);
			}
		}
		$data = AppDotNet::sendReply($data['Message'], $data['PostID'], $images);
		if($data->meta->code == 200) {
			$link = self::join_links($this->Link(), 'thread', $data->data->id, '#a' . $data->data->id);
			if($this->request->requestVar('embed')) {
				$link = self::join_links($link, '?embed=1');
			}
			$postData = array(
				'blob' => $this->postToData($data->data)->renderWith('Post'),
				'under' => isset($data->data->reply_to) ? $data->data->reply_to : null,
				'link' => $link,
			);
			$postData = json_encode($postData, JSON_HEX_QUOT | JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS);
			Requirements::insertHeadTags(<<<HEAD
<noscript>
<meta http-equiv="refresh" content="2;url=$link">
</noscript>
HEAD
);
			Requirements::customScript(<<<JS
if(typeof window.parent.postReturn === "function") {
	window.parent.postReturn($postData);
} else {
	window.parent.location = "$link";
}
JS
);
			$data = array(
				'Form' => DBField::create_field('HTMLText', '<h3>Posted!</h3>')
			);
		} else {
			$data = array(
				'Form' => 'Unable to post. Got: ' . $data->meta->error_message
			);
		}
		return $this->renderWith('TreePage_reply', $data);
	}

	public function doThread($data) {
		$id = $data['postID'];
		$id = urldecode($id);
		$id = preg_replace('#(https?://)?((alpha\.app\.net/[^/]+/post/)|posts\.app\.net/)#i', '', $id);
		$id = preg_replace('/(\?|#).*/', '', $id);
		$link = self::join_links($this->Link(), 'thread', $id, '#a' . $id);
		return $this->redirect($link);
	}

	public function Tree() {
		if($this->request->param('ID')) {
			$res = AppDotNet::getThread($this->request->param('ID'));
			if(!is_array($res)) { $this->httpError(500, $res->error_message); exit(); }
			list($tree, $posts, $children) = $res;
			if(isset($tree->code)) {
				return $this->httpError($tree->code, $tree->error_message);
			}
			foreach($posts as $p) {
				$this->cache->set('post-' . $p->id, $p);
			}
			if(!empty($posts[$this->request->param('ID')])) {
				$post = $posts[$this->request->param('ID')];
				if(!empty($post->repost_of)) {
					$id = $post->repost_of->id;
					$link = self::join_links($this->Link(), 'thread', $id, '#a' . $id);
					return $this->redirect($link);
				}
			}
			return '<ul class="treeUL">' . $this->treeToUl($tree) . '</ul>';
		}
	}

	protected function treeToUl($tree, $depth=1, $belowNode = false) {
		$classes = array('depth-' . $depth, );
		if($tree->children) {
			$classes[] = 'node';
			if($tree->id != $this->request->param('ID') && $depth > 1 && empty($tree->forceOpen) && (!$belowNode || $belowNode > 1)) {
				$classes[] = 'closed';
			} else {
				$classes[] = 'open';
			}
			if(count($tree->children) == 1) {
				$classes[] = 'single-child';
			}
		} else {
			$classes[] = 'leaf';
		}
		$ul = '<li class="' . implode(' ', $classes) . '">';
		$ul .= $this->nodeToHTML($tree);
		if($tree->children) {
			$ul .= "<ul>\n";
			$nextBelow = $belowNode ? $belowNode + 1 : ($tree->id == $this->request->param('ID') ? 1 : false);
			foreach($tree->children as $child) {
				$ul .= $this->treeToUl($child, $depth+1, $nextBelow);
			}
			$ul .= "</ul>\n";
		}
		$ul .= "</li>\n";
		return $ul;
	}

	protected function getPost($id, $annotations = false) {
		$prefix = $annotations ? 'post-a-' : 'post-';
		$post = isset($_GET['flush']) ? false : $this->cache->get($prefix . $id);
		if(!$post) {
			$post = AppDotNet::getPost($id, (bool)$annotations);
			if($post) {
				$this->cache->set($prefix . $post->id, $post);
			}
		}
		return $post;
	}

	protected function getUser() {
		if(!AppDotNet::hasUserToken()) {
			return false;
		}
		$user = $this->cache->get('user-' . Session::get('App.Net.UserToken'));
		if(!$user) {
			$user = AppDotNet::getUser();
			if($user) {
				$this->cache->set('user-' . Session::get('App.Net.UserToken'), $user);
			}
		}
		return $user;
	}

	public function reply(SS_HTTPRequest $req) {
		if(AppDotNet::hasUserToken()) {
			if($req->param('ID') == -1) {
				$form = $this->ReplyForm();
				if(!$form->Fields()->fieldByName('PostID')->value) $form->loadDataFrom(array('PostID' => $req->param('ID')));
				return array(
					'Form' => $form
				);
			}
			$post = $this->getPost($req->param('ID'));
			if(!$post) {
				return 'No such post, or the post has been deleted';
			}
			$mentions = array($post->user->username);
			foreach($post->entities->mentions as $mention) {
				$mentions[] = $mention->name;
			}
			$mentions = array_unique($mentions);
			if(count($mentions) > 1) {
				$user = $this->getUser();
				if(($key = array_search($user->username, $mentions)) !== false) {
					unset($mentions[$key]);
				}
			}
			$message = '@' . implode(' @', $mentions) . ' ';
			$form = $this->ReplyForm();
			if(!empty($post->repost_of)) {
				$post = $post->repost_of;
			}
			if(!$form->Fields()->fieldByName('PostID')->value) $form->loadDataFrom(array('PostID' => $post->id, 'Message' => $message));
			$replying_to = new ArrayList();
			do {
				$replying_to->push($this->postToData($post));
				if(isset($post->reply_to) && $post->reply_to) {
					$post = $this->getPost($post->reply_to);
				} else {
					$post = null;
				}
			} while($post);
			return array(
				'Form' => $form,
				'Thread' => $replying_to
			);
		} else {
			$return = $this->AbsoluteLink('login/' . $req->param('ID'));
			if($this->request->requestVar('embed')) {
				$return .= '?embed=1';
			}
			$url = AppDotNet::getUserTokenURL($return, self::$default_scopes, SecurityToken::getSecurityID());
			return $this->redirect($url);
		}
	}

	public function login(SS_HTTPRequest $req) {
		// Protection against CSRF attacks
		$token = SecurityToken::inst();
		$token->setName('state');
		if(!$token->checkRequest($req)) {
			$this->httpError(400, "Sorry, your session has timed out. Please close this popup and try again.");
		}
		$return = $this->AbsoluteLink('login/' . $req->param('ID'));
		if($this->request->requestVar('embed')) {
			$return .= '?embed=1';
		}
		$token = AppDotNet::handleUserTokenReturn($req, $return);
		if(!$token) {
			return 'An error occurred while try to get access. Please close this popup and try again.';
		} else {
			$return = $this->Link('reply/' . $req->param('ID'));
			if($this->request->requestVar('embed')) {
				$return .= '?embed=1';
			}
			return $this->redirect($return);
		}
	}
}

class TreeFormVal extends Validator {
	public function php($data) {
		if(empty($data['postID']) || !trim($data['postID'])) {
			$this->validationError(
				'postID',
				'A post ID is required',
				'required'
			);
			return;
		}
		$id = $data['postID'];
		$id = urldecode($id);
		$id = preg_replace('#(https?://)?((alpha\.app\.net/[^/]+/post/)|posts\.app\.net/)#i', '', $id);
		$id = preg_replace('/(\?|#).*/', '', $id);
		if(!AppDotNet::hasPost($id)) {
			$this->validationError('postID', 'Either that post doesn\'t exist or App.Net is having problems.', 'error');
		}
	}
}

class PostFormVal extends Validator {
	public function php($data) {
		if(empty($data['PostID']) || !trim($data['PostID'])) {
			$this->validationError(
				'PostIDWrap',
				'A post ID is required',
				'required'
			);
			return;
		}
		$id = $data['PostID'];
		if(!AppDotNet::hasPost($id) && $id != -1) {
			$this->validationError('PostIDWrap', 'Either that post doesn\'t exist or App.Net is having problems.', 'error');
		}
		if(empty($_POST['imageIDs']) && (empty($data['Message']) || !trim($data['Message']))) {
			$this->validationError('Message', 'A message is required', 'required');
		} elseif(mb_strlen($data['Message']) > 256) {
			$this->validationError('Message', 'A message can only be 256 characters', 'error');
		}
	}
}
