<?php

class User extends DataObject implements TemplateGlobalProvider {
	private static $db = array(
		'ADNToken' => 'Varchar(255)',
		'PocketToken' => 'Varchar(255)',
		'PocketName' => 'Varchar(255)',
		'ShowMentions' => 'Boolean',
		'ShowDirectedMentions' => 'Boolean',
		'UserID' => 'Varchar',
		'LoginHash' => 'Varchar(255)',
		'HasMessages' => 'Boolean',
	);

	private static $indexes = array(
		'UserID' => true,
	);

	private static $defaults = array(
		'ShowMentions' => true,
		'HasMessages' => true,
	);

	public function HasPocket() {
		return (bool)$this->PocketToken;
	}

	public function getPocketButton() {
		if($this->exists()) {
			if($this->HasPocket()) {
				$token = SecurityToken::inst();
				$removeURL = Controller::join_links('PocketController', 'RemovePocket');
				$removeURL = $token->addToUrl($removeURL);

				return 'Connected to Pocket account ' . $this->PocketName . '. <a href="' . $removeURL . '">Disconnect</a>';
			} else {
				return '<a href="PocketController/Connect">Connect to Pocket</a>';
			}
		}
	}

	public function generateCookieHash() {
		$hasher = new RandomGenerator;
		do {
			$hash = substr($hasher->randomToken() . $hasher->randomToken(), 0, 255);
		} while(User::get()->filter('LoginHash', $hash)->Count() > 0);
		$this->LoginHash = $hash;
		$this->write();
		Cookie::set('TreeView', $this->LoginHash, 180, null, null, false, true);
	}

	public static function get_user() {
		$user = static::get_user_from_cookie();
		if(!$user) {
			$user = static::get_user_from_token();
		}
		return $user;
	}

	public static function get_user_from_token() {
		$user = AppDotNet::getUser();
		if($user) {
			$user = static::get_user_from_id($user->id);
			$user->ADNToken = Session::get('App.Net.UserToken');
		}
		return $user;
	}

	public static function get_user_from_id($id) {
		$obj = static::get()->filter('UserID', $id)->First();
		if(!$obj) {
			$obj = new User;
			$obj->ADNToken = Session::get('App.Net.UserToken');
			$obj->UserID = $id;
		}
		return $obj;
	}

	public static function get_user_from_cookie() {
		$user = $value = Cookie::get('TreeView');
		if($value) {
			$user = static::get()->filter('LoginHash:case', $value)->First();
		}
		if($user) {
			Session::set('App.Net.UserToken', $user->ADNToken);
		}
		return $user;
	}

	public static function get_template_global_variables() {
		return array(
			'TVUser' => 'get_user',
		);
	}
}
