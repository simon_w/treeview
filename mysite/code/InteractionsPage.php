<?php

class InteractionsPage extends Page {
	private static $db = array(

	);

	private static $has_one = array(

	);
}

class InteractionsPage_Controller extends Page_Controller {
	private static $allowed_actions = array(
		'index',
		'login',
	);

	protected function interactionToData($data) {
		$action = array('Type' => $data->action);
		switch($action['Type']) {
			case 'reply':
			case 'repost':
			case 'star':
				$action['Post'] = $this->postToData($data->objects[0]);
				break;
			case 'follow':
				$action['User'] = $this->userToData($data->objects[0]);
				break;
		}
		$action['Users'] = new ArrayList;
		foreach($data->users as $user) {
			$action['Users']->push($this->userToData($user));
		}
		return new ArrayData($action);
	}

	protected function userToData($data) {
		$user = new ArrayData($data);

		return $user;
	}

	public function index(SS_HTTPRequest $req) {
		if(!AppDotNet::hasUserToken()) {
			$return = $this->AbsoluteLink('login');
			$url = AppDotNet::getUserTokenURL($return, self::$default_scopes, SecurityToken::getSecurityID());
			$button = "<a href='$url' title='Login with App.net'><button>Login with App.net</button></a>";
			return $this->renderWith('Page', array(
				'Form' => DBField::create_field('HTMLText', $button)
			));
		}
		return array();
	}

	public function Interactions() {
		$data = AppDotNet::getData('https://alpha-api.app.net/stream/0/users/me/interactions?count=200');
		$posts = new ArrayList();
		foreach($data->data as $post) {
			$posts->push($this->interactionToData($post));
		}
		return $posts;
	}

	public function login(SS_HTTPRequest $req) {
		// Protection against CSRF attacks
		$token = SecurityToken::inst();
		$token->setName('state');
		if(!$token->checkRequest($req)) {
			$this->httpError(400, "Sorry, your session has timed out. Please close this popup and try again.");
		}
		$return = $this->AbsoluteLink('login');
		$token = AppDotNet::handleUserTokenReturn($req, $return);
		if(!$token) {
			return 'An error occurred while try to get access. Please close this popup and try again.';
		} else {
			return $this->redirect($this->AbsoluteLink());
		}
	}
}
