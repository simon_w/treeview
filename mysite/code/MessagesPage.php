<?php

class MessagesPage extends Page {
	private static $db = array(
		'LoggedinContent' => 'HTMLText',
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->addFieldToTab('Root.Main', new HTMLEditorField('LoggedinContent', 'Logged in content'));
		return $fields;
	}
}

class MessagesPage_Controller extends Page_Controller {
	protected $channelID;
	protected $broadcast;

	private static $allowed_actions = array(
		'index',
		'login',
		'load',
		'channel',
		'ReplyForm',
		'Form',
	);

	public function init() {
		parent::init();
		Requirements::javascript('themes/simple/javascript/scroll.js');
	}

	public function index(SS_HTTPRequest $req) {
		if(!AppDotNet::hasUserToken()) {
			$return = $this->AbsoluteLink('login/');
			$url = AppDotNet::getUserTokenURL($return, self::$default_scopes, SecurityToken::getSecurityID());
			$button = "<a href='$url' title='Login with App.net'><button>Login with App.net</button></a>";
			return $this->renderWith('Page', array(
				'Form' => DBField::create_field('HTMLText', $button)
			));
		}
		if(!$this->user->HasMessages) {
			$return = $this->AbsoluteLink('login/');
			$url = AppDotNet::getUserTokenURL($return, self::$default_scopes, SecurityToken::getSecurityID());
			$button = "<a href='$url' title='Allow access to messages'><button>Allow access to messages</button></a>";
			return $this->renderWith('Page', array(
				'Form' => DBField::create_field('HTMLText', $button)
			));
		}
		return array();
	}

	public function Content() {
		if(!AppDotNet::hasUserToken() || !$this->user->HasMessages) {
			return $this->Content;
		}
		return $this->LoggedinContent;
	}

	public function Menu($level = 0) {
		if($this->user && $level == 2) {
			if(!$this->user->HasMessages) return;
			$datas = AppDotNet::pagedGet('https://alpha-api.app.net/stream/0/channels?channel_types=net.app.core.pm,net.patter-app.room,net.app.core.broadcast&include_channel_annotations=1');
			if(empty($datas)) return;
			$links = array();
			$wantedUsers = [];
			foreach($datas as $channel) {
				$d = array(
					'Link' => $this->link('channel/' . $channel->id),
				);
				if($channel->id == $this->request->param('ID') &&
					$this->request->param('Action') == 'channel') {
					$d['LinkingMode'] = 'current';
				} elseif($channel->has_unread) {
					$d['LinkingMode'] = 'link unread';
				} else {
					$d['LinkingMode'] = 'link';
				}
				if($channel->type == 'net.patter-app.room') {
					$title = 'Patter room';
					foreach($channel->annotations as $a) {
						if($a->type == 'net.patter-app.settings' && !empty($a->value->name)) {
							$title = $a->value->name;
							break;
						}
					}
					if($channel->writers->any_user || $channel->writers->public) {
						$title .= ' (public)';
					} elseif($channel->readers->any_user || $channel->readers->public) {
						$title .= ' (public read)';
					} else {
						$title .= ' (private)';
					}
				} elseif($channel->type == 'net.app.core.broadcast') {
					$title = 'Broadcast';
					foreach($channel->annotations as $a) {
						if($a->type == 'net.app.core.broadcast.metadata' && !empty($a->value->title)) {
							$title = $a->value->title . ' (broadcast)';
							break;
						}
					}
				} else {
					$ids = $channel->writers->user_ids;
					if(!$ids) $ids = array();
					$ids[] = $channel->owner->id;
					$ids = array_unique($ids);
					if(($key = array_search($this->user->UserID, $ids)) !== false) {
						unset($ids[$key]);
					}
					$wantedUsers = array_unique(array_merge($wantedUsers, $ids));
					if(!$ids) $ids = 'Me (pm)';
					$title = $ids;
				}
				$d['Title'] = $d['MenuTitle'] = $title;
				$links[] = $d;
			}
			if($wantedUsers) {
				$names = [];
				foreach(array_chunk($wantedUsers, 200) as $ids) {
					$ids = implode(',', $ids);
					$url = 'https://alpha-api.app.net/stream/0/users?ids=' . $ids;
					$users = AppDotNet::getData($url);
					foreach($users->data as $u) {
						$names[$u->id] = $u->name;
					}
				}
				foreach($links as $i => $d) {
					if(is_array($d['Title'])) {
						$tNames = [];
						foreach($d['Title'] as $id) {
							$tNames[] = $names[$id];
						}
						$d['Title'] = $d['MenuTitle'] = implode(', ', $tNames) . ' (pm)';
					}
					$links[$i] = new ArrayData($d);
				}
			}
			return new ArrayList($links);
		}
		return parent::Menu($level);
	}

	public function channel(SS_HTTPRequest $req) {
		if(!AppDotNet::hasUserToken()) {
			$return = $this->AbsoluteLink('login/' . $req->param('ID'));
			$url = AppDotNet::getUserTokenURL($return, self::$default_scopes, SecurityToken::getSecurityID());
			$button = "<a href='$url' title='Login with App.net'><button>Login with App.net</button></a>";
			return $this->renderWith('Page', array(
				'Form' => DBField::create_field('HTMLText', $button)
			));
		}
		if(!$this->user->HasMessages) {
			$return = $this->AbsoluteLink('login/' . $req->param('ID'));
			$url = AppDotNet::getUserTokenURL($return, self::$default_scopes, SecurityToken::getSecurityID());
			$button = "<a href='$url' title='Allow access to messages'><button>Allow access to messages</button></a>";
			return $this->renderWith('Page', array(
				'Form' => DBField::create_field('HTMLText', $button)
			));
		}
		$id = (int)$req->param('ID');
		$channel = AppDotNet::getData('https://alpha-api.app.net/stream/0/channels/' . $id . '?include_marker=1');
		if($channel->meta->code != 200) {
			return $this->httpError($channel->meta->code, $channel->meta->error_message);
		}
		$this->channelID = $id;
		$this->broadcast = $channel->data->type == 'net.app.core.broadcast';
		if($req->param('OtherID') == 'load') {
			$req->shift(1);
			return $this->handleRequest($req, $this->model);
		}
		if(isset($channel->data->recent_message_id)) {
			$data = array(
				'name' => $channel->data->marker->name,
				'id' => $channel->data->recent_message_id,
			);
			AppDotNet::postData('https://alpha-api.app.net/stream/0/posts/marker', Convert::raw2json($data));
		}
		if($channel->data->writers->you) {
			if($channel->data->type == 'net.app.core.broadcast') {
				$form = DBField::create_field('HTMLText', 'This is a broadcast room. Please visit <a href="https://broadcast.app.net/manage/' . $channel->data->id . '/">here</a> to post to it.</a>');
			} else {
				$form = $this->ReplyForm();
				$form->loadDataFrom(array('ChannelID' => $id));
			}
		} else {
			$form = 'This room is read-only. You can watch, but not chat.';
		}
		return array('ReplyForm' => $form);
	}

	public function load(SS_HTTPRequest $req) {
		$this->before = $req->param('ID');
		if(!AppDotNet::hasUserToken()) {
			return '';
		}
		return $this->renderWith('Messages');
	}

	public function Mentions() {
		$id = $this->channelID;
		if($this->broadcast) {
			$data = AppDotNet::getData('https://alpha-api.app.net/stream/0/channels/' . $id . '/messages?count=25&include_deleted=0&include_annotations=1&include_machine=1&before_id=' . $this->before);
		} else {
			$data = AppDotNet::getData('https://alpha-api.app.net/stream/0/channels/' . $id . '/messages?count=25&include_deleted=0&include_annotations=1&before_id=' . $this->before);
		}
		$posts = new ArrayList();
		foreach($data->data as $post) {
			$posts->push($this->messageToData($post));
		}
		$this->LastPost = false;
		if(!$data->meta->more) {
			if($posts->Count()) {
				$posts->Last()->extraClass .= ' last';
			}
			$this->LastPost = true;
		}
		return $posts;
	}

	public function ReplyForm() {
		$fields = new FieldList(
			new TextAndImageField('Message', false),
			new HiddenField('ChannelID', false),
			new LiteralField('clearing', '<div class="clear"><!-- --></div>'),
			new LiteralField('charsLeft', '<span id="charsLeft" data-base=2048>2048</span>')
		);
		$actions = new FieldList(
			new FormAction('doReply', 'Post')
		);
		return new Form($this, __FUNCTION__, $fields, $actions);
	}

	public function Form() {
		$fields = new FieldList(
			$f = new TextField('Users', 'To:'),
			new TextareaField('Message', 'Message:')
		);
		$f->setRightTitle('A comma separated list of usernames. At least one has to be someone that isn\'t you.');
		$actions = new FieldList(
			new FormAction('doNew', 'Post')
		);
		return new Form($this, __FUNCTION__, $fields, $actions);
	}

	public function doReply($data, $form) {
		$id = (int)$data['ChannelID'];
		$url = 'https://alpha-api.app.net/stream/0/channels/' . $id;
		$channel = AppDotNet::getData($url);
		if($channel->meta->code != 200) {
			return $this->httpError($channel->meta->code, $channel->meta->error_message);
		}
		$url .= '/messages';
		$ids = $data['imageIDs'];
		$images = array();
		if($ids) {
			$ids = explode(',', $ids);
			$ids = array_map('intval', $ids);
			foreach($ids as $i) {
				$token = Session::get('TextAndImageField.Token.' . $i);
				if(!$token) {
					continue;
				}
				$images[] = array(
					'file_id' => $i,
					'file_token' => $token,
					'format' => 'oembed',
				);
			}
		}
		$data = AppDotNet::sendMessage($data['Message'], $data['ChannelID'], $images);
		if($data->meta->code == 200) {
			$link = self::join_links($this->Link(), 'channel', $data->data->channel_id);
			return $this->redirect($link);
		} else {
			Session::clear("FormInfo.{$form->FormName()}");
			$form->sessionMessage($data->meta->error_message, 'error');
			return $this->redirectBack();
		}
	}

	public function donew($data, $form) {
		$url = 'https://alpha-api.app.net/stream/0/channels/pm/messages';
		$d = array(
			'text' => $data['Message'],
			'destinations' => array(),
		);
		foreach(explode(',', $data['Users']) as $user) {
			$user = '@' . trim($user);
			$d['destinations'][] = $user;
		}
		$data = AppDotNet::postData($url, json_encode($d, JSON_HEX_QUOT | JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS));
		if($data->meta->code == 200) {
			$link = self::join_links($this->Link(), 'channel', $data->data->channel_id);
			return $this->redirect($link);
		} else {
			Session::set("FormInfo.{$form->FormName()}.message", $data->meta->error_message);
			Session::set("FormInfo.{$form->FormName()}.type", 'error');
			return $this->redirectBack();
		}
	}

	public function login(SS_HTTPRequest $req) {
		// Protection against CSRF attacks
		$token = SecurityToken::inst();
		$token->setName('state');
		if(!$token->checkRequest($req)) {
			$this->httpError(400, "Sorry, your session has timed out. Please try again.");
		}
		$return = $this->AbsoluteLink('login/' . $req->param('ID'));
		$token = AppDotNet::handleUserTokenReturn($req, $return);
		if(!$token) {
			return 'An error occurred while try to get access. Please try again.';
		} else {
			$user = User::get_user_from_token();
			$user->HasMessages = true;
			$user->write();
			if($req->param('ID')) return $this->redirect($this->AbsoluteLink('channel/' . $req->param('ID')));
			return $this->redirect($this->AbsoluteLink());
		}
	}
}
