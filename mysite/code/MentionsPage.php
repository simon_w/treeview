<?php

class MentionsPage extends Page {
	private static $db = array(

	);

	private static $has_one = array(

	);
}

class MentionsPage_Controller extends Page_Controller {
	private static $allowed_actions = array(
		'index',
		'login',
		'load',
	);

	protected static $require_user = true;

	public function index(SS_HTTPRequest $req) {
		if(!AppDotNet::hasUserToken()) {
			$return = $this->AbsoluteLink('login');
			$url = AppDotNet::getUserTokenURL($return, self::$default_scopes, SecurityToken::getSecurityID());
			$button = "<a href='$url' title='Login with App.net'><button>Login with App.net</button></a>";
			return $this->renderWith('Page', array(
				'Form' => DBField::create_field('HTMLText', $button)
			));
		}
		return array();
	}

	protected $before = '';

	public function load(SS_HTTPRequest $req) {
		$this->before = $req->param('ID');
		if(static::$require_user && !AppDotNet::hasUserToken()) {
			return '';
		}
		return $this->renderWith('Mentions');
	}

	public function Mentions() {
		$data = AppDotNet::getData('https://alpha-api.app.net/stream/0/users/me/mentions?count=25&include_post_annotations=1&include_deleted=0&before_id=' . $this->before);
		$posts = new ArrayList();
		foreach($data->data as $post) {
			$posts->push($this->postToData($post));
		}
		$this->LastPost = false;
		if(!$data->meta->more) {
			$posts->Last()->extraClass .= ' last';
			$this->LastPost = true;
		}
		return $posts;
	}

	public function login(SS_HTTPRequest $req) {
		// Protection against CSRF attacks
		$token = SecurityToken::inst();
		$token->setName('state');
		if(!$token->checkRequest($req)) {
			$this->httpError(400, "Sorry, your session has timed out. Please close this popup and try again.");
		}
		$return = $this->AbsoluteLink('login');
		$token = AppDotNet::handleUserTokenReturn($req, $return);
		if(!$token) {
			return 'An error occurred while try to get access. Please close this popup and try again.';
		} else {
			return $this->redirect($this->AbsoluteLink());
		}
	}
}
