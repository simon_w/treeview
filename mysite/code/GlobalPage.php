<?php

class GlobalPage extends MentionsPage {
	private static $db = array(

	);

	private static $has_one = array(

	);
}

class GlobalPage_Controller extends MentionsPage_Controller {
	private static $allowed_actions = array(
		'index',
		'login',
		'load',
		'explore',
		'all',
	);

	protected $streams;

	protected static $require_user = false;

	public function init() {
		parent::init();
		$this->streams = $this->cache->get('ExploreStreams');
		if(!$this->streams) {
			$this->streams = AppDotNet::pagedGet('https://alpha-api.app.net/stream/0/posts/stream/explore');
			$keys = array_map(function($thing) { return $thing->slug; }, $this->streams);
			$this->streams = array_combine($keys, $this->streams);
			$this->cache->set('ExploreStreams', $this->streams, 86400);
		}
	}

	public function index(SS_HTTPRequest $req) {
		return $this->redirect($this->Link('all'));
	}

	public function all(SS_HTTPRequest $req) {
		$this->url = 'https://alpha-api.app.net/stream/0/posts/stream/global?count=25&include_post_annotations=1&include_deleted=0&before_id=';
		$this->localAction = 'all';
		if($req->param('ID') == 'load') {
			$req->shiftAllParams();
			return $this->handleRequest($req, $this->model);
		}
		return array();
	}

	public function explore(SS_HTTPRequest $req) {
		$this->slug = $req->param('ID');
		$this->localAction = 'explore';
		if($req->param('OtherID') == 'load') {
			$req->shift(1);
			return $this->handleRequest($req, $this->model);
		}
		return array();
	}

	public function Mentions() {
		if($this->localAction == 'all') {
			$data = AppDotNet::getData($this->url . $this->before);
		} else {
			$slug = $this->slug;
			if(!isset($this->streams[$slug])) {
				return $this->httpError(404);
			}
			$url = $this->streams[$slug]->url;
			$url = self::join_links($url, '?count=25&include_post_annotations=1&include_deleted=0&before_id=' . $this->before);
			$data = AppDotNet::getData($url);
		}
		$posts = new ArrayList();
		foreach($data->data as $post) {
			$posts->push($this->postToData($post));
		}
		$this->LastPost = false;
		if(!$data->meta->more) {
			$posts->Last()->extraClass .= ' last';
			$this->LastPost = true;
		}
		return $posts;
	}

	public function Menu($level = 0) {
		if($level == 2) {
			if(empty($this->streams)) return;
			$links = array();
			$d = array(
				'Link' => $this->Link('all'),
				'Title' => 'Global',
				'MenuTitle' => 'Global',
				'HoverTitle' => DBField::create_field('Varchar', 'All posts'),
			);
			if($this->request->param('Action') == 'all') {
				$d['LinkingMode'] = 'current';
			} else {
				$d['LinkingMode'] = 'link';
			}
			$links[] = new ArrayData($d);
			foreach($this->streams as $stream) {
				$d = array(
					'Link' => $this->link('explore/' . $stream->slug),
					'HoverTitle' => DBField::create_field('Varchar', $stream->description),
				);
				if($stream->slug == $this->request->param('ID') &&
					$this->request->param('Action') == 'explore') {
					$d['LinkingMode'] = 'current';
				} else {
					$d['LinkingMode'] = 'link';
				}
				$d['Title'] = $d['MenuTitle'] = $stream->title;
				$links[] = new ArrayData($d);
			}
			return new ArrayList($links);
		}
		return parent::Menu($level);
	}
}
