<?php

class SSViewerIterator implements TemplateIteratorProvider {

	protected $iteratorPos;
	protected $iteratorTotalItems;

	public static function get_template_iterator_variables() {
		return array(
			'SecondLast',
		);
	}

	/**
	 * Set the current iterator properties - where we are on the iterator.
	 *
	 * @param int $pos position in iterator
	 * @param int $totalItems total number of items
	 */
	public function iteratorProperties($pos, $totalItems) {
		$this->iteratorPos        = $pos;
		$this->iteratorTotalItems = $totalItems;
	}

	/**
	 * Returns true if this object is the second to last in a set.
	 *
	 * @return bool
	 */
	public function SecondLast() {
		return $this->iteratorPos == $this->iteratorTotalItems - 2;
	}
}
