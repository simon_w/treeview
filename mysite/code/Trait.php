<?php

trait adn {
	protected $cache;

	protected $user;

	protected static $default_scopes = array(
		'stream',
		'messages',
		'basic',
		'write_post',
		'follow',
		'files',
	);

	public function HasUser() {
		return (bool)$this->user;
	}

	protected function adnINIT() {
		$this->cache = new Memcached;
		$this->cache->addServer('localhost', 11211);
		$this->cache->setOption(Memcached::OPT_PREFIX_KEY, 'ADN-');

		$this->user = User::get_user();
		if($this->user) {
			$this->user->generateCookieHash();
		}
	}

	protected function postToData($node) {
		$pageLink = $this->TreePage();
		if(!empty($node->repost_of)) {
			$data = $this->postToData($node->repost_of);
			$data->reposter = $node->user;
			$data->link = $pageLink->Link('reply/' . $node->id);
			if($this->HasUser()) {
				if($node->repost_of->user->id != $this->user->UserID) {
					$data->starLink = $pageLink->Link('star/' . $node->repost_of->id . '/' . (int)$node->repost_of->you_starred);
					$data->starred = (bool)$node->repost_of->you_starred;
					$data->repostLink = $pageLink->Link('repost/' . $node->repost_of->id . '/' . (int)$node->repost_of->you_reposted);
					$data->reposted = (bool)$node->repost_of->you_reposted;
				}
			}
			if($this->request->requestVar('embed')) {
				$data->link .= '?embed=1';
			}
			return $data;
		}
		$content = $this->textToContent($node);
		$time = DBField::create_field('Datetime', strtotime($node->created_at));
		if($node->id == $this->request->param('ID')) {
			$extraClass = ' highlight';
		} else {
			$extraClass = '';
		}
		$link = $pageLink->Link('reply/' . $node->id);
		$threadLink = Controller::join_links($pageLink->Link('thread'), $node->id, '#a' . $node->id);

		$thumbnails = array();

		if(!empty($node->annotations)) foreach($node->annotations as $annot) {
			if($annot->type != 'net.app.core.oembed') {
				continue;
			}
			if($annot->value->type != 'photo') {
				continue;
			}
			if(empty($annot->value->thumbnail_url) || empty($annot->value->embeddable_url)) {
				continue;
			}
			if(empty($annot->value->thumbnail_width) || empty($annot->value->thumbnail_height)) {
				continue;
			}
			$thumbnails[] = new ArrayData(array(
				'URL' => $annot->value->thumbnail_url,
				'Link' => $annot->value->embeddable_url,
				'Width' => max((int)($annot->value->thumbnail_width/2), 100),
				'Height' => max((int)($annot->value->thumbnail_height/2), 100),
			));
		}

		$data = new ArrayData($node);
		$data->link = $link;
		$data->ThreadLink = $threadLink;
		$data->extraClass = $extraClass;
		$data->time = $time;
		$data->content = $content;
		$data->HasThread = $node->num_replies || $node->thread_id != $node->id;
		$data->Thumbs = new ArrayList($thumbnails);

		if($this->HasUser()) {
			$data->starLink = $pageLink->Link('star/' . $node->id . '/' . (int)$node->you_starred);
			$data->starred = (bool)$node->you_starred;
			if($node->user->id != $this->user->UserID) {
				$data->repostLink = $pageLink->Link('repost/' . $node->id . '/' . (int)$node->you_reposted);
				$data->reposted = (bool)$node->you_reposted;
			}
		}

		if($this->request->requestVar('embed')) {
			$data->link .= '?embed=1';
			$data->ThreadLink .= '?embed=1';
		}
		return $data;
	}

	protected function messageToData($node, $broadcast = false) {
		$pageLink = $this;
		$content = $this->textToContent($node);
		$time = DBField::create_field('Datetime', strtotime($node->created_at));
		$link = $pageLink->Link('reply/' . $node->id);
		$threadLink = Controller::join_links($pageLink->Link('channel'), $node->channel_id);

		$thumbnails = array();

		$subject = '';
		$sub_link = '';

		if(!empty($node->annotations)) foreach($node->annotations as $annot) {
			if($annot->type == 'net.app.core.broadcast.message.metadata') {
				$subject = $annot->value->subject;
				continue;
			}
			if($annot->type == 'net.app.core.crosspost') {
				$sub_link = $annot->value->canonical_url;
				continue;
			}
			if($annot->type != 'net.app.core.oembed') {
				continue;
			}
			if($annot->value->type != 'photo') {
				continue;
			}
			if(empty($annot->value->thumbnail_url) || (empty($annot->value->embeddable_url) && empty($annot->value->url))) {
				continue;
			}
			if(empty($annot->value->thumbnail_width) || empty($annot->value->thumbnail_height)) {
				continue;
			}
			$thumbnails[] = new ArrayData(array(
				'URL' => $annot->value->thumbnail_url,
				'Link' => empty($annot->value->embeddable_url) ? $annot->value->url : $annot->value->embeddable_url,
				'Width' => max((int)($annot->value->thumbnail_width/2), 100),
				'Height' => max((int)($annot->value->thumbnail_height/2), 100),
			));
		}

		$data = new ArrayData($node);
		$data->link = $link;
		$data->ChannelLink = $threadLink;
		$data->time = $time;
		$data->content = $content;
		$data->Thumbs = new ArrayList($thumbnails);
		$data->Subject = DBField::create_field('Text', $subject);
		$data->SubLink = DBField::create_field('Text', $sub_link);
		return $data;
	}

	protected function nodeToHTML($node) {
		$data = $this->postToData($node);
		return $data->renderWith('Post');
	}

	protected function textToContent($node) {
		if(!empty($node->is_deleted)) {
			return '<em>[Post deleted]</em>';
		}
		// var_dump($node);die();
		if(empty($node->text)) {
			$base = '';
		} else {
			$base = $node->text;
		}
		$base = str_replace(array('  ', "\xA0 ", "\n "), array("\xA0 ", "\xA0\xA0", "\n\xA0"), $base);
		$content = $base;
		$i = 0;
		$key = str_pad('!', 256, $i++) . '!';
		$replaces = array(
			$key => '&nbsp;',
			"\t" => '<span class="tabholder">	</span>',
		);
		$entities = array();
		foreach($node->entities->hashtags as $e) {
			$e->type = 'hashtag';
			$entities[] = $e;
		}
		foreach($node->entities->links as $e) {
			$e->type = 'link';
			$entities[] = $e;
		}
		foreach($node->entities->mentions as $e) {
			$e->type = 'mention';
			$entities[] = $e;
		}
		usort($entities, function($a, $b) {
			return $b->pos - $a->pos;
		});
		$content = $base;
		foreach($entities as $e) {
			$left = mb_substr($content, 0, $e->pos);
			$middle = mb_substr($content, $e->pos, $e->len);
			$right = mb_substr($content, $e->pos + $e->len);
			switch($e->type) {
				case 'hashtag':
					$link = 'https://alpha.app.net/hashtags/' . $e->name;
					$title = $e->name;
					break;
				case 'mention':
					$link = 'https://alpha.app.net/' . $e->name;
					$title = $e->name;
					break;
				case 'link':
					$link = $e->url;
					$title = $e->text;
					break;
			}
			$leftKey = str_pad('!', 256, sprintf('%03d', $i++)) . '!';
			$rightKey = str_pad('!', 256, sprintf('%03d', $i++)) . '!';
			$replaces[$leftKey] = sprintf('<a target="_blank" href="%s" title="%s">', $link, htmlspecialchars($title, ENT_QUOTES | ENT_HTML5, 'UTF-8'));
			$replaces[$rightKey] = '</a>';
			if($e->type == 'link' && $this->user && $this->user->HasPocket()) {
				$replaces[$rightKey] .= ' <img src="themes/simple/images/pocket.png" class="pocketLink" data-href="' . $link . '" title="Save to Pocket">';
			}
			$content = sprintf('%s%s%s%s%s', $left, $leftKey, $middle, $rightKey, $right);
		}

		$replaces['[pocket.co] (via <a target="_blank" href="https://alpha.app.net/pocket" title="pocket">@Pocket</a>)'] = '(via <a target="_blank" href="https://alpha.app.net/pocket" title="pocket">@Pocket</a>)';

		$content = str_replace("\xA0", $key, $content);
		$content = htmlspecialchars($content, ENT_QUOTES | ENT_IGNORE | ENT_HTML5, 'UTF-8');
		$content = str_ireplace(array_keys($replaces), array_values($replaces), $content);
		return nl2br($content);
	}
}
