<?php

class SettingsPage extends Page {
	private static $db = array();

	private static $has_one = array();
}

class SettingsPage_Controller extends Page_Controller {
	private static $allowed_actions = array(
		'Form',
		'login',
	);

	public function index(SS_HTTPRequest $req) {
		if(!AppDotNet::hasUserToken()) {
			$return = $this->AbsoluteLink('login');
			$url = AppDotNet::getUserTokenURL($return, self::$default_scopes, SecurityToken::getSecurityID());
			$button = "<a href='$url' title='Login with App.net'><button>Login with App.net</button></a>";
			return $this->renderWith('Page', array(
				'Form' => DBField::create_field('HTMLText', $button)
			));
		}
		return array();
	}

	public function login(SS_HTTPRequest $req) {
		// Protection against CSRF attacks
		$token = SecurityToken::inst();
		$token->setName('state');
		if(!$token->checkRequest($req)) {
			$this->httpError(400, "Sorry, your session has timed out. Please close this popup and try again.");
		}
		$return = $this->AbsoluteLink('login');
		$token = AppDotNet::handleUserTokenReturn($req, $return);
		if(!$token) {
			return 'An error occurred while try to get access. Please close this popup and try again.';
		} else {
			return $this->redirect($this->AbsoluteLink());
		}
	}

	public function Form() {
		$fields = new FieldList(
			new CheckboxField('ShowDirectedMentions', 'Show posts @directed to users I don\'t follow'),
			new CheckboxField('ShowMentions', 'Show mentions in My Stream'),
			$f = new ReadonlyField('PocketButton', ' ')
		);
		$f->dontEscape = true;
		$actions = new FieldList(
			new FormAction('saveSettings', 'Save')
		);
		$form = new Form($this, __FUNCTION__, $fields, $actions);
		$form->loadDataFrom($this->user);
		return $form;
	}

	public function saveSettings($data, $form) {
		$form->saveInto($this->user);
		$this->user->write();
		return $this->redirect($this->AbsoluteLink());
	}
}
