<?php

class SearchPage extends MentionsPage {
	private static $db = array(

	);

	private static $has_one = array(

	);
}

class SearchPage_Controller extends MentionsPage_Controller {
	private static $allowed_actions = array(
		'index',
		'login',
		'load',
	);

	protected $streams;

	protected static $require_user = false;

	public function index(SS_HTTPRequest $req) {
		if(!$req->getVar('q')) {
			return $this->redirect(GlobalPage::get()->First()->Link());
		}
		return array();
	}

	public function Mentions() {
		$url = sprintf('https://alpha-api.app.net/stream/0/posts/search?text=%s&count=25&before_id=', rawurlencode($this->request->getVar('q')));
		$data = AppDotNet::getData($url . $this->before);
		$posts = new ArrayList();
		foreach($data->data as $post) {
			$posts->push($this->postToData($post));
		}
		$this->LastPost = false;
		if(!$data->meta->more) {
			$posts->Last()->extraClass .= ' last';
			$this->LastPost = true;
		}
		return $posts;
	}
}
