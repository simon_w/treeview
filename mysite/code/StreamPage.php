<?php

class StreamPage extends MentionsPage {
	private static $db = array(

	);

	private static $has_one = array(

	);
}

class StreamPage_Controller extends MentionsPage_Controller {
	private static $allowed_actions = array(
		'index',
		'login',
		'load',
	);

	public function Mentions() {
		$url = 'https://alpha-api.app.net/stream/0/posts/stream%s?count=25&include_deleted=0&before_id=%s&include_post_annotations=1&include_directed_posts=%d';
		$url = sprintf($url, $this->user->ShowMentions ? '/unified' : '', $this->before, $this->user->ShowDirectedMentions);
		$data = AppDotNet::getData($url);
		$posts = new ArrayList();
		foreach($data->data as $post) {
			$posts->push($this->postToData($post));
		}
		$this->LastPost = false;
		if(!$data->meta->more) {
			$posts->Last()->extraClass .= ' last';
			$this->LastPost = true;
		}
		return $posts;
	}
}
