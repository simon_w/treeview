<?php

class AppDotNet {
	const clientID = 'f7AUb7Akar3WbnUWbpfkgUDAZF777tLT';
	const clientSecret = 'S4j8Ma9DHQLx6bEArevqwrkPnDNDhzvY';

	public static function getAccessToken() {
		if(self::hasUserToken()) {
			return Session::get('App.Net.UserToken');
		}
		$token = Session::get('App.Net.Token');
		if(!$token) {
			$curl = curl_init('https://alpha.app.net/oauth/access_token');
			$postBody = sprintf('client_id=%s&client_secret=%s&grant_type=client_credentials', self::clientID, self::clientSecret);
			curl_setopt_array($curl, array(
				CURLOPT_POST => true,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POSTFIELDS => $postBody,
			));
			$data = curl_exec($curl);
			curl_close($curl);
			$data = json_decode($data);
			$token = $data->access_token;
			Session::set('App.Net.Token', $token);
		}
		return $token;
	}

	protected static function createTree($base) {
		$tree = clone $base;
		$tree->children = array();
		foreach(self::$children[$base->id] as $child) {
			$tree->children[] = self::createTree(self::$posts[$child]);
		}
		return $tree;
	}

	private static $ch = false;
	public static function getCH($headers = array()) {
		if(!self::$ch) {
			self::$ch = curl_init();
			curl_setopt_array(self::$ch, array(
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_HEADER => true,
			));
		}
		$headers = array_merge(array(
			'Authorization: Bearer ' . self::getAccessToken(),
			'Connection: Keep-Alive',
			'Keep-Alive: 300',
		), $headers);

		curl_setopt(self::$ch, CURLOPT_HTTPHEADER, $headers);
		return self::$ch;
	}

	public static function getData($url, $includeHeaders = false) {
		$curl = self::getCH();
		curl_setopt_array($curl, array(
			CURLOPT_HTTPGET => true,
			CURLOPT_URL => $url
		));
		$data = curl_exec($curl);
		if(!$data) {
			Controller::curr()->httpError(500);
		}
		list($headers, $data) = explode("\r\n\r\n", $data, 2);
		$data = json_decode($data);
		if($includeHeaders) {
			$data->headers = array();
			foreach(explode("\r\n", $headers) as $header) {
				if(!$header || strpos($header, ':') === false) continue;
				list($name, $value) = explode(':', $header, 2);
				$data->headers[trim($name)] = trim($value);
			}
		}
		return $data;
	}

	public static function postData($url, $data, $includeHeaders = false) {
		$headers = array();
		if($data && $data[0] == '{') {
			$headers[] = 'Content-Type: application/json';
		}
		$curl = self::getCH($headers);
		curl_setopt_array($curl, array(
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $data,
			CURLOPT_URL => $url,
		));

		$data = curl_exec($curl);
		list($headers, $data) = explode("\r\n\r\n", $data, 2);
		$data = json_decode($data);
		if($includeHeaders) {
			$data->headers = array();
			foreach(explode("\r\n", $headers) as $header) {
				if(!$header || strpos($header, ':') === false) continue;
				list($name, $value) = explode(':', $header, 2);
				$data->headers[trim($name)] = trim($value);
			}
		}
		return $data;
	}

	public static function deleteData($url, $includeHeaders = false) {
		$curl = self::getCH();
		curl_setopt_array($curl, array(
			CURLOPT_CUSTOMREQUEST => 'DELETE',
			CURLOPT_URL => $url
		));
		$data = curl_exec($curl);
		if(!$data) {
			Controller::curr()->httpError(500);
		}
		list($headers, $data) = explode("\r\n\r\n", $data, 2);
		$data = json_decode($data);
		if($includeHeaders) {
			$data->headers = array();
			foreach(explode("\r\n", $headers) as $header) {
				if(!$header || strpos($header, ':') === false) continue;
				list($name, $value) = explode(':', $header, 2);
				$data->headers[trim($name)] = trim($value);
			}
		}
		return $data;
	}

	protected static $posts = array();
	protected static $children = array();
	protected static $base = false;

	protected static function handlePost($post) {
		self::$posts[$post->id] = $post;
		if(isset($post->reply_to) && $post->reply_to) {
			self::$children[$post->reply_to][] = $post->id;
		}
		if($post->id == $post->thread_id) {
			self::$base = $post;
		}
		if(!isset(self::$children[$post->id])) {
			self::$children[$post->id] = array();
		}
	}

	public static function pagedGet($url, $count = 200, $until = 0, $minID = false, $maxID = false) {
		if($minID && $maxID) {
			$posts = self::pagedGet($url, $count, $until, $minID);
			return array_merge($posts, self::pagedGet($url, $count, $until, false, $maxID));
		}
		if(!$minID && !$maxID) {
			$add = sprintf('?count=%d&include_deleted=1', $count);
		} elseif($minID) {
			$add = sprintf('?count=%d&include_deleted=1&before_id=%s', $count, $minID);
		} else {
			$add = sprintf('?count=%d&include_deleted=1&since_id=%s', $count, $maxID);
		}
		$dataURL = Controller::join_links($url, $add);
		$data = self::getData($dataURL);
		if(!isset($data->data)) {
			return array();
		}
		$posts = $data->data ?: array();
		if($data->data && !empty($data->meta->more)) {
			if($minID) {
				// only go down down down
				$extra = self::pagedGet($url, $count, $until, $data->meta->min_id);
			} elseif($maxID) {
				// only go up up up
				$extra = self::pagedGet($url, $count, $until, false, $data->meta->max_id);
			} else {
				// Both ways!
				$extra = self::pagedGet($url, $count, $until, $data->meta->min_id, $data->meta->max_id);
			}
			return array_merge($posts, $extra);
		}
		return $posts;
	}

	protected static function loadThread($postID, $minID = false, $maxID = false) {
		// var_dump(func_get_args());
		if($minID && $maxID) {
			$error = self::loadThread($postID, $minID);
			if($error) return $error;
			return self::loadThread($postID, false, $maxID);
		}
		if(!$minID && !$maxID) {
			$url = sprintf('https://alpha-api.app.net/stream/0/posts/%s/replies?count=200&include_post_annotations=1&include_deleted=1', $postID);
		} elseif($minID) {
			$url = sprintf('https://alpha-api.app.net/stream/0/posts/%s/replies?count=200&include_post_annotations=1&include_deleted=1&before_id=%s', $postID, $minID);
		} else {
			$url = sprintf('https://alpha-api.app.net/stream/0/posts/%s/replies?count=200&include_post_annotations=1&include_deleted=1&since_id=%s', $postID, $maxID);
		}
		$data = self::getData($url);
		if(!isset($data->data)) {
			return $data->meta;
		}
		foreach($data->data as $post) {
			self::handlePost($post);
		}
		if($data->data && $data->meta->more) {
			if($minID) {
				// only go down down down
				return self::loadThread($postID, $data->meta->min_id);
			} elseif($maxID) {
				// only go up up up
				return self::loadThread($postID, false, $data->meta->max_id);
			} else {
				// Both ways!
				return self::loadThread($postID, $data->meta->min_id, $data->meta->max_id);
			}
		}
	}

	public static function getThread($postID) {
		$error = self::loadThread($postID);
		if($error) {
			return $error;
		}
		foreach(self::$children as &$child) {
			sort($child);
		}
		$post = self::$posts[$postID];
		if(!empty($post->reply_to)) {
			$post = self::$posts[$post->reply_to];
		} else {
			$post = null;
		}
		while($post) {
			$post->forceOpen = true;
			if(!empty($post->reply_to)) {
				$post = self::$posts[$post->reply_to];
			} else {
				$post = null;
			}
		}
		return array(self::createTree(self::$base), self::$posts, self::$children);
	}

	protected static function loadCommentThread($channelID, $minID = false, $maxID = false) {
		// var_dump(func_get_args());
		if($minID && $maxID) {
			$error = self::loadCommentThread($channelID, $minID);
			if($error) return $error;
			return self::loadCommentThread($channelID, false, $maxID);
		}
		if(!$minID && !$maxID) {
			$url = sprintf('https://alpha-api.app.net/stream/0/channels/%s/messages?count=200&include_message_annotations=1&include_deleted=1', $postID);
		} elseif($minID) {
			$url = sprintf('https://alpha-api.app.net/stream/0/channels/%s/messages?count=200&include_message_annotations=1&include_deleted=1&before_id=%s', $postID, $minID);
		} else {
			$url = sprintf('https://alpha-api.app.net/stream/0/channels/%s/messages?count=200&include_message_annotations=1&include_deleted=1&since_id=%s', $postID, $maxID);
		}
		$data = self::getData($url);
		if(!isset($data->data)) {
			return $data->meta;
		}
		foreach($data->data as $post) {
			self::handlePost($post);
		}
		if($data->data && $data->meta->more) {
			if($minID) {
				// only go down down down
				return self::loadThread($postID, $data->meta->min_id);
			} elseif($maxID) {
				// only go up up up
				return self::loadThread($postID, false, $data->meta->max_id);
			} else {
				// Both ways!
				return self::loadThread($postID, $data->meta->min_id, $data->meta->max_id);
			}
		}
	}

	public static function getCommentThread($channelID) {
		$error = self::loadCommentThread($channelID);
		if($error) {
			return $error;
		}
		foreach(self::$children as &$child) {
			sort($child);
		}
		return array(self::createTree(self::$base), self::$posts, self::$children);
	}

	public static function hasPost($postID) {
		return (bool)self::getPost($postID);
	}

	public static function getPost($postID, $annotations = false, $deleted = false) {
		$url = sprintf('https://alpha-api.app.net/stream/0/posts/%s?include_deleted=%d&include_annotations=%d', $postID, $annotations, $deleted);
		$data = self::getData($url);
		if($data->meta->code == 200) {
			return $data->data;
		}
		return false;
	}

	public static function getUser() {
		if(!self::hasUserToken()) {
			return false;
		}
		$data = self::getData('https://alpha-api.app.net/stream/0/users/me');
		if($data->meta->code == 200) {
			return $data->data;
		}
		return false;
	}

	public static function sendReply($post, $replyTo, $images = false) {
		$data = array(
			'text' => $post,
			'reply_to' => $replyTo
		);
		if($images) {
			$annotations = array();
			foreach($images as $image) {
				$annotations[] = array(
					'type' => 'net.app.core.oembed',
					'value' => array(
						'+net.app.core.file' => $image,
					),
				);
			}
			$data['annotations'] = $annotations;
			if(mb_strlen($post) + (count($images)*4) + 2 <= 256) {
				// Get the actual entities
				if($post) $newData = self::postData('https://alpha-api.app.net/stream/0/text/process', json_encode($data, JSON_HEX_QUOT | JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS), true);
				if(!$post || $newData->meta->code == 200) {
					$links = array();
					if($post) {
						foreach($newData->data->entities->links as $l) {
							$links[] = array(
								'len' => $l->len,
								'pos' => $l->pos,
								'url' => $l->url,
							);
						}
						$data['text'] .= ' – ';
					}
					$c = 0;
					foreach($images as $i) {
						$links[] = array(
							'len' => 3,
							'pos' => mb_strlen($data['text']),
							'url' => 'https://photos.app.net/{post_id}/' . (++$c)
						);
						$data['text'] .= '<=> ';
					}
					$data['text'] = substr($data['text'], 0, -1);
					$data['entities'] = array('links' => $links);
				}
			}
		}
		if($replyTo == -1) unset($data['reply_to']);
		return self::postData('https://alpha-api.app.net/stream/0/posts', json_encode($data, JSON_HEX_QUOT | JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS));
	}

	public static function sendMessage($post, $channelID, $images = false) {
		$data = array(
			'text' => $post
		);
		if($images) {
			$annotations = array();
			foreach($images as $image) {
				$annotations[] = array(
					'type' => 'net.app.core.oembed',
					'value' => array(
						'+net.app.core.file' => $image,
					),
				);
			}
			$data['annotations'] = $annotations;
		}
		$url = 'https://alpha-api.app.net/stream/0/channels/' . $channelID . '/messages';
		return self::postData($url, json_encode($data, JSON_HEX_QUOT | JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS));
	}

	public static function hasUserToken() {
		return (bool)Session::get('App.Net.UserToken');
	}

	public static function getUserTokenURL($return, array $scopes, $state = false) {
		$url = 'https://alpha.app.net/oauth/authenticate?client_id=%s&response_type=code&redirect_uri=%s&scope=%s';
		if($state) {
			$url .= '&state=%s';
		}
		return sprintf($url, self::clientID, urlencode($return), implode(' ', $scopes), $state);
	}

	public static function handleUserTokenReturn(SS_HTTPRequest $req, $return) {
		$code = $req->getVar('code');
		if(!$code || $req->getVar('error')) return;
		$curl = curl_init('https://alpha.app.net/oauth/access_token');
		$postBody = 'client_id=%s&client_secret=%s&grant_type=authorization_code&redirect_uri=%s&code=%s';
		$postBody = sprintf($postBody, self::clientID, self::clientSecret, urlencode($return), $code);
		curl_setopt_array($curl, array(
			CURLOPT_POST => true,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POSTFIELDS => $postBody
		));
		$data = curl_exec($curl);
		curl_close($curl);
		$data = json_decode($data);
		if(!$data || !isset($data->access_token)) {
			return;
		}
		$token = $data->access_token;
		Session::set('App.Net.UserToken', $token);
		return $token;
	}
}
