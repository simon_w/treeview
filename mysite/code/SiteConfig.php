<?php

class SCExt extends DataExtension {
	private static $db = array(
		'FooterContent' => 'HTMLText',
	);

	public function updateCMSFields(FieldList $fields) {
		$data = AppDotNet::getData('https://alpha-api.app.net/stream/0/posts', true);
		$fields->addFieldsToTab('Root.Main', array(
			new ReadonlyField('X-RateLimit-Remaining', 'Client rate limit remaining', $data->headers['X-RateLimit-Remaining']),
			new ReadonlyField('X-RateLimit-Limit', 'Client rate limit max', $data->headers['X-RateLimit-Limit']),
			new ReadonlyField('X-RateLimit-Reset', 'Client rate limit reset', date('Y-m-d H:i:s', time() + $data->headers['X-RateLimit-Reset']))
		));
		$fields->addFieldToTab('Root.Main', new HTMLEditorField('FooterContent', 'Footer content'));
	}
}
