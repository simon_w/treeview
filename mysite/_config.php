<?php

date_default_timezone_set('UTC');

global $project;
$project = 'mysite';

global $database;
$database = 'treeview';

require_once('conf/ConfigureFromEnv.php');

// Set the site locale
i18n::set_locale('en_US');


require_once __DIR__ . '/code/Trait.php';
